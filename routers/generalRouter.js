const Router = require('koa-router')

const getBackendLanding = async (ctx) => {
    ctx.body = 'Welcome to the Nebus Backend! v1.0.0';
    ctx.status = 200;
}

const generalRouter = new Router({ prefix: '/'});

generalRouter.get('/', getBackendLanding);

module.exports = generalRouter;