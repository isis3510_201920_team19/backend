const Router = require('koa-router');
const Joi = require('@hapi/joi');
const errorHandler = require('./errorHandler');
const userService = require('../services/userService');

const createUser = async (ctx) => {
    const schema = Joi.object({
        firebaseId: Joi.string()
            .length(28)
            .required()
    });
    const user = errorHandler.validateSchema(schema, ctx.request.body);

    const createdUser = await userService.createUser(user);
    ctx.body = createdUser;
}

const createThesisUser = async (ctx) => {
    const schema = Joi.object({
        email: Joi.string()
            .email()
            .required(),
        inviterEmail: Joi.string()
            .email()
    })
    const thesisUser = errorHandler.validateSchema(schema, ctx.request.body);
    if (thesisUser.email === thesisUser.inviterEmail) {
        throw new Error('email and inviterEmail cannot be the same');
    }

    const createdThesisUser = await userService.createThesisUser(thesisUser);
    ctx.body = createdThesisUser;
}

const userRouter = new Router();

userRouter.post('/user', errorHandler.handleErrors, createUser);
userRouter.post('/thesis-user', errorHandler.handleErrors, createThesisUser);

module.exports = userRouter;