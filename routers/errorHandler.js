exports.handleErrors = async (ctx, next) => {
    try{
        await next();
    } catch (err) {
        ctx.status = err.status || 500;
        ctx.body = { errorMessage: err.message };
        ctx.app.emit('error', err, ctx);
    }
};

exports.validateSchema = (schema, object) => {
    const { value, error } = schema.validate(object);
    if (error) {
        throw new Error(error.message);
    } else {
        return value;
    }
}