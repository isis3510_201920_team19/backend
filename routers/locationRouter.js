const Router = require('koa-router');
const Joi = require('@hapi/joi');
const errorHandler = require('./errorHandler');
const locationService = require('../services/locationService');

const createLocations = async (ctx) => {
    const schema = Joi.object({
        userId: Joi.number()
            .integer()
            .positive()
            .required(),
        busRouteId: Joi.string()
            .max(30)
            .required(),
        locations: Joi.array().items(
            Joi.object({
                timestamp: Joi.date()
                    .iso()
                    .required(),
                latitude: Joi.number()
                    .precision(5)
                    .min(-90)
                    .max(90)
                    .required(),
                longitude: Joi.number()
                    .precision(5)
                    .min(-180)
                    .max(180)
                    .required(),
                horizontalAccuracy: Joi.number()
                    .integer()
                    .required(),
                course: Joi.number()
                    .integer()
                    .min(-1)
                    .max(360)
                    .required(),
                speed: Joi.number()
                    .precision(2)
                    .required()
            })
        ).min(1)
    });
    const data = errorHandler.validateSchema(schema, ctx.request.body);

    await locationService.createLocations(data);
    ctx.body = { message: 'locations added successfully' };
}

const createThesisLocations = async (ctx) => {
    const schema = Joi.object({
        userId: Joi.number()
            .integer()
            .positive()
            .required(),
        locations: Joi.array().items(
            Joi.object({
                timestamp: Joi.date()
                    .iso()
                    .required(),
                latitude: Joi.number()
                    .precision(5)
                    .min(-90)
                    .max(90)
                    .required(),
                longitude: Joi.number()
                    .precision(5)
                    .min(-180)
                    .max(180)
                    .required(),
                horizontalAccuracy: Joi.number()
                    .integer()
                    .required(),
                course: Joi.number()
                    .integer()
                    .min(-1)
                    .max(360)
                    .required(),
                speed: Joi.number()
                    .precision(2)
                    .required()
            })
        ).min(1)
    });
    const data = errorHandler.validateSchema(schema, ctx.request.body);

    await locationService.createThesisLocations(data);
    ctx.body = { message: 'locations added successfully' };
}

const getRecentThesisLocations = async (ctx) => {
    const locations = await locationService.getRecentThesisLocations();
    ctx.body = locations;
}

const locationRouter = new Router();

locationRouter.post('/locations', errorHandler.handleErrors, createLocations);
locationRouter.post('/thesis-locations', errorHandler.handleErrors, createThesisLocations);
locationRouter.get('/recent-thesis-locations', errorHandler.handleErrors, getRecentThesisLocations);

module.exports = locationRouter;