const Router = require('koa-router');
const Joi = require('@hapi/joi');
const errorHandler = require('./errorHandler');
const waitTimeService = require('../services/waitTimeService');

const createWaitTime = async (ctx) => {
    const schema = Joi.object({
        userId: Joi.number()
            .integer()
            .positive()
            .required(),
        busStopName: Joi.string()
            .max(30)
            .required(),
        startTimestamp: Joi.date()
            .iso()
            .required(),
        waitTimeSeconds: Joi.number()
            .integer()
            .positive()
            .required()
    });
    const waitTime = errorHandler.validateSchema(schema, ctx.request.body);

    await waitTimeService.createWaitTime(waitTime);
    ctx.body = { message: 'wait time added successfully' };
}

const createThesisWaitTime = async (ctx) => {
    const schema = Joi.object({
        userId: Joi.number()
            .integer()
            .positive()
            .required(),
        busStopName: Joi.string()
            .max(30)
            .required(),
        startTimestamp: Joi.date()
            .iso()
            .required(),
        waitTimeSeconds: Joi.number()
            .integer()
            .positive()
            .required()
    });
    const waitTime = errorHandler.validateSchema(schema, ctx.request.body);

    await waitTimeService.createThesisWaitTime(waitTime);
    ctx.body = { message: 'wait time added successfully' };
}

const waitTimeRouter = new Router();

waitTimeRouter.post('/wait-time', errorHandler.handleErrors, createWaitTime);
waitTimeRouter.post('/thesis-wait-time', errorHandler.handleErrors, createThesisWaitTime);

module.exports = waitTimeRouter;