const Router = require('koa-router');
const errorHandler = require('./errorHandler');
const busRouteService = require('../services/busRouteService');

const getBusRoutes = async (ctx) => {
    const busRoutes = await busRouteService.getBusRoutes();
    ctx.body = { busRoutes: busRoutes };
}

const busRouteRouter = new Router();

busRouteRouter.get('/bus-routes', errorHandler.handleErrors, getBusRoutes);

module.exports = busRouteRouter;