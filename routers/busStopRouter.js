const Router = require('koa-router');
const Joi = require('@hapi/joi');
const errorHandler = require('./errorHandler');
const busStopService = require('../services/busStopService');

const getBusRouteBusStops = async (ctx) => {
    console.log(ctx.params)
    const schema = Joi.object({
        busRouteId: Joi.number()
    })
    const busRoute = errorHandler.validateSchema(schema, ctx.params);

    const busStops = await busStopService.getBusRouteBusStops(busRoute.busRouteId);
    ctx.body = { busStops: busStops };
}

const busStopRouter = new Router();

busStopRouter.get('/bus-routes/:busRouteId/bus-stops', errorHandler.handleErrors, getBusRouteBusStops);

module.exports = busStopRouter;