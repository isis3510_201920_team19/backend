const Koa = require('koa');
const logger = require('koa-logger');
const helmet = require('koa-helmet');
const bodyParser = require('koa-bodyparser');

// config
const PORT = process.env.PORT || 3000;

// API Routers
const generalRouter = require('./routers/generalRouter');
const userRouter = require('./routers/userRouter');
const busRouteRouter = require('./routers/busRouteRouter');
const busStopRouter = require('./routers/busStopRouter');
const locationRouter = require('./routers/locationRouter');
const waitTimeRouter = require('./routers/waitTimeRouter');

const app = new Koa();

app.use(logger());
app.use(helmet());
app.use(bodyParser());

app.use(generalRouter.routes());
app.use(generalRouter.allowedMethods());

app.use(userRouter.routes());
app.use(userRouter.allowedMethods());

app.use(busRouteRouter.routes());
app.use(busRouteRouter.allowedMethods());

app.use(busStopRouter.routes());
app.use(busStopRouter.allowedMethods());

app.use(locationRouter.routes());
app.use(locationRouter.allowedMethods());

app.use(waitTimeRouter.routes());
app.use(waitTimeRouter.allowedMethods());

const server = app.listen(PORT);
module.exports = server;