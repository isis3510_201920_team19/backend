const db = require('../db');
const busStopService = require('./busStopService');

exports.createWaitTime = async (waitTimeData) => {
    const busStopId = await busStopService.getBusStopId(waitTimeData.busStopName);
    await db.query(`INSERT INTO wait_times (user_id, bus_stop_id, start_ts, wait_time_s) VALUES (${waitTimeData.userId}, ${busStopId}, TIMESTAMP '${waitTimeData.startTimestamp.toUTCString().slice(0, -4)}', ${waitTimeData.waitTimeSeconds})`);
}

exports.createThesisWaitTime = async (waitTime) => {
    await db.query(`INSERT INTO thesis_wait_times (user_id, bus_stop, start_ts, wait_time_s) VALUES (${waitTime.userId}, '${waitTime.busStopName}', TIMESTAMP '${waitTime.startTimestamp.toUTCString().slice(0, -4)}', ${waitTime.waitTimeSeconds})`);
}