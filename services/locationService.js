const db = require('../db');

exports.createLocations = async (locationsData) => {
    let userId = locationsData.userId;
    let busRouteId = locationsData.busRouteId;

    let values = '';
    locationsData.locations.forEach(location => {
        values +=  `(${userId}, '${busRouteId}', TIMESTAMP '${location.timestamp.toUTCString().slice(0, -4)}', ${location.latitude}, ${location.longitude}, ${location.horizontalAccuracy}, ${location.course}, ${location.speed}), `;
    });
    values = values.slice(0, -2);

    await db.query(`INSERT INTO locations (user_id, bus_route_id, ts, latitude, longitude, horizontal_accuracy, course, speed) VALUES ${values}`);
}

exports.createThesisLocations = async (locationsData) => {
    let userId = locationsData.userId;

    let values = '';
    locationsData.locations.forEach(location => {
        values +=  `(${userId}, TIMESTAMP '${location.timestamp.toUTCString().slice(0, -4)}', ${location.latitude}, ${location.longitude}, ${location.horizontalAccuracy}, ${location.course}, ${location.speed}), `;
    });
    values = values.slice(0, -2);

    await db.query(`INSERT INTO thesis_locations (thesis_user_id, ts, latitude, longitude, horizontal_accuracy, course, speed) VALUES ${values}`);
}

exports.getRecentThesisLocations = async () => {
    const now = new Date();
    now.setMinutes(now.getMinutes() - 5);
    now.setHours(now.getHours() - 5);

    const result = await db.query(`SELECT thesis_user_id AS bus_id, ts, latitude, longitude, horizontal_accuracy, course, speed FROM (SELECT DISTINCT ON (thesis_user_id) thesis_user_id, ts, latitude, longitude, horizontal_accuracy, course, speed FROM (SELECT * FROM thesis_locations WHERE TIMESTAMP '${now.toString().slice(0, -34)}' <= ts) AS tl ORDER BY thesis_user_id, ts DESC) AS result;`);

    const locations = {
        locations: []
    }
    if (0 < result.rowCount) {
        locations['locations'] = result.rows;
    }
    return locations;
}