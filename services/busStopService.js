const db = require('../db');

exports.getBusStopId = async (busStopName) => {
    const result = await db.query(`SELECT id FROM bus_stops WHERE bus_stop_name = '${busStopName}'`);
    if (result.rows.length < 1) {
        throw new Error('bus stop does not exist');
    }

    const userId = result.rows[0].id;
    return userId;
}

exports.getBusRouteBusStops = async (busRouteId) => {
    const result = await db.query(`SELECT id, name, latitude, longitude FROM bus_stops AS a INNER JOIN (SELECT * FROM bus_routes_bus_stops WHERE bus_route_id = ${busRouteId}) AS b ON a.id = b.bus_stop_id`);
    return result.rows;
}