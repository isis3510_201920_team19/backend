const db = require('../db');

exports.getBusRoutes = async () => {
    const result = await db.query(`SELECT * FROM bus_routes`);
    return result.rows;
}