const db = require('../db');

exports.createUser = async ({ firebaseId }) => {
    const result = await db.query(`SELECT * FROM users WHERE firebase_id = '${firebaseId}'`);
    if (0 < result.rowCount) {
        return result.rows[0];
    } else {
        await db.query(`INSERT INTO users (firebase_id) VALUES ('${firebaseId}')`);

        const verificationResult = await db.query(`SELECT * FROM users WHERE firebase_id = '${firebaseId}'`);
        return verificationResult.rows[0];
    }
}

exports.createThesisUser = async ({ email, inviterEmail }) => {
    const result = await db.query(`SELECT * FROM thesis_users WHERE email = '${email}'`)
    if (0 < result.rowCount) {
        return result.rows[0];
    } else {
        if (inviterEmail) {
            await db.query(`INSERT INTO thesis_users (email, inviter_email) VALUES ('${email}', '${inviterEmail}')`);
        } else {
            await db.query(`INSERT INTO thesis_users (email) VALUES ('${email}')`);
        }
        const verificationResult = await db.query(`SELECT * FROM thesis_users WHERE email = '${email}'`);
        return verificationResult.rows[0];
    }
}