const { Pool, types } = require('pg');

// Pool connection setup
var pool = new Pool({
    user: process.env.PGUSER || 'root',
    host: process.env.PGHOST || 'localhost',
    database: process.env.PGDATABASE || 'nebus',
    password: process.env.PGPASSWORD || 'password',
    port: process.env.PGPORT || 5432,
});

types.setTypeParser(1114, function(stringValue) {
    return stringValue;
});

module.exports = {
    query: (text, params) => {
        return pool.query(text, params);
    }
}